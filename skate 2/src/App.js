 import {BrowserRouter, Route} from 'react-router-dom';
import Report from './components/report/report'; 

import "react-toastify/dist/ReactToastify.css";
import InvPayment from './components/invPayment/invPayment';
import InvoiceData from './components/invoiceData/invoiceData';
import SuccessPayment from './components/successPayment/successPayment';
 function App() {
  return (
    <div className="App">
      <BrowserRouter>
     
      <Route exact path="/report" component={Report}/>
      <Route exact path="/invPayment" component={InvPayment}/>
      <Route exact path="/Invoice" component={InvoiceData}/>
     <Route exact path="/successPayment" component={SuccessPayment}/>
 
      </BrowserRouter>

     </div>
  );
}

export default App;
