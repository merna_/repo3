import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
 import './invoice.css';

const Invoice=()=>{
    useEffect(()=>{
        window.scrollTo(0,0)
    })
    return(
        <div className="invoice">
            <div className="invoice-header">
                <ul>
                    <li>
                        <div className="invoiceName">
                            <h5>Invoice# 289734567</h5>
                            <p>Pending</p>
                        </div>
                    </li>
                    <li className="right">
                        <div className="invoiceImg">
                            {/* img */}
                        </div>
                    </li>
                </ul>
            </div>
            <div className="invoiceBody">
                <div className="row">
                         <p className="to">To:</p>
                        <span>Michael Jones - Texas Auto Transport</span>
                    </div>
                    <div className="row from">
                         <p>From:</p>
                        <span>John Doe - Ryets Inc.</span>
                    </div>
                </div>
                <div className="total">
                <div className="row ">
                         <p>Total:  </p>
                        <span> $2,520.00</span>
                    </div>
                </div>
                <div className="border"></div>
                <div className="viewInvoice">
                    <Link to="/invoice" target="_blank">
                         <span>View Invoice 
                         <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="14" height="14" viewBox="0 0 14 14"fill="#0078d4"><defs><clipPath id="a"><rect className="a" width="14" height="14" transform="translate(206.525 319)"/></clipPath></defs><g className="b" transform="translate(-206.525 -319)"><g transform="translate(206.525 319)"><path className="c" d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"/><path className="c" d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"/></g></g></svg>
                             </span>
                         </Link>
                </div>
        </div>
    )
}
export default Invoice