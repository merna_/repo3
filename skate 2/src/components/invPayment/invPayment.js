import React, { useEffect } from 'react';
import Invoice from './invoice/invoice';
import './invPayment.css';
import PaymentHeader from './payment/paymentHeader';

const InvPayment=()=>{
    useEffect(()=>{
        window.scrollTo(0,0)
    })
    return(
        <div className="invPayment">
            <div className="container">
                <div className="row">
                    <div className="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                        <Invoice/>
                    </div>
                    <div className="col-lg-6  col-md-12 col-sm-12 col-xs-12">
                        <PaymentHeader/>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default InvPayment