import React from "react";
import "./invoiceFooter.css";

const InvoiceFooter = () => {
  
  return (
    <div className="invoiceFooter">
      <p>Copyright © 2021 Ryets. All rights reserved.</p>
    </div>
  );
};
export default InvoiceFooter;
