import React from "react";
import "./mapInfo.css";
import mapboxgl from "mapbox-gl/dist/mapbox-gl.js";
import mapImg from '../../../images/map.png'
mapboxgl.accessToken =
  "pk.eyJ1IjoibWVybmEtZXp6MjgiLCJhIjoiY2tkenRwNTc5MzliZDJzb2QzenhudWx0eCJ9.GUBl5uOpfVc8FNFBudHjiA ";

class MapInfo extends React.Component {
  state = {
    pickup: -79.51016257714582,
    pickup2: 43.702399671175385,
  };
  componentDidMount() {
    var map = new mapboxgl.Map({
      container: "map",
      style: "mapbox://styles/merna-ezz28/ckia7b400b13k19lrl5bifocf",
      center: [-79.4512, 43.6568], // starting position
      zoom: 12.7,
    });

    map.flyTo({
      center: [-79.58486038106673, 43.576508365604354],
      essential: true, // this animation is considered essential with respect to prefers-reduced-motion

      bounds: [
        [-79.57202033691038, 43.56536806783515],
        [-79.58486038106673, 43.576508365604354],
      ],
      pitch: false,
      bearing: false,
    });

    var start = [-79.57202033691038, 43.56536806783515];

    const getRoute = (end) => {
      // make a directions request using cycling profile
      // an arbitrary start will always be the same
      // only the end or destination will change
      var start = [-79.57202033691038, 43.56536806783515];
      // driving, driving-traffic
      var url =
        "https://api.mapbox.com/directions/v5/mapbox/driving-traffic/" +
        start[0] +
        "," +
        start[1] +
        ";" +
        end[0] +
        "," +
        end[1] +
        "?steps=true&geometries=geojson&access_token=" +
        mapboxgl.accessToken;

      // make an XHR request https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
      var req = new XMLHttpRequest();

      req.open("GET", url, true);
      req.onload = function () {
        var json = JSON.parse(req.response);
        document.getElementById("distances").innerHTML = (
          json.routes[0].distance /
          1.609 /
          1000
        ).toFixed(2);
        var data = json.routes[0];
        var route = data.geometry.coordinates;

        var geojson = {
          type: "Feature",
          properties: {},
          geometry: {
            type: "LineString",
            coordinates: route,
          },
        };
        // if the route already exists on the map, reset it using setData
        if (map.getSource("route")) {
          map.getSource("route").setData(geojson);
        } else {
          // otherwise, make a new request
          map.addLayer({
            id: "route",
            type: "line",
            source: {
              type: "geojson",
              data: {
                type: "Feature",
                properties: {},
                geometry: {
                  type: "LineString",
                  coordinates: geojson,
                },
              },
            },
            layout: {
              "line-join": "round",
              "line-cap": "round",
            },
            paint: {
              "line-color": "#0078D4",
              "line-width": 1,
              "line-opacity": 0.75,
            },
          });
        }
        // add turn instructions here at the end
      };
      req.send();
    };

    map.on("load", function () {
      // make an initial directions request that
      // starts and ends at the same location
      getRoute(start);
      map.loadImage(mapImg, function (error, image) {
        if (error) throw error;
        map.addImage("cat", image);
        map.addSource("point", {
          type: "geojson",
          data: {
            type: "FeatureCollection",
            features: [
              {
                type: "Feature",
                geometry: {
                  type: "Point",
                  coordinates: start,
                },
              },
            ],
          },
        });
        map.addLayer({
          id: "routes",
          type: "symbol",
          source: "point",
          layout: {
            "icon-image": "cat",
            "icon-size": 0.45,
          },
        });
      });
    });
    // this is where the code from the next step will go
    map.on("load", function () {
      var coords = [-79.58486038106673, 43.576508365604354];
      var end = {
        type: "FeatureCollection",
        features: [
          {
            type: "Feature",
            properties: {},
            geometry: {
              type: "Point",
              coordinates: coords,
            },
          },
        ],
      };
      if (map.getLayer("end")) {
        map.getSource("end").setData(end);
      } else {
        map.loadImage(mapImg, function (error, image2) {
          if (error) throw error;
          map.addImage("cat2", image2);
          map.addSource("point2", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [
                {
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: coords,
                  },
                },
              ],
            },
          });
          map.addLayer({
            id: "end",
            type: "symbol",
            source: "point2",
            layout: {
              "icon-image": "cat2",
              "icon-size": 0.45,
            },
          });
        });
      }
      getRoute(coords);
    });
  }
  render() {
    return (
      <div className="map">
        <div id="map" className="mapboxgl-map mapInfo">
          <div className="container mapData">
            <div className="row">
              <div className="col-md-8 col-sm-9 col-12">
                <div className="col-md-12" style={{ display: "flex" }}>
                  <div className="col-md-5 col-sm-5 col-6">
                    <div className="from">
                      <div>AutoDeal, inc</div>
                      <div>Austin, TX</div>
                    </div>
                  </div>
                  <div className="col-md-2 col-sm-2 col-2">
                    <svg
                      className="img1"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlnsxlink="http://www.w3.org/1999/xlink"
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="#1c1c26"
                    >
                      <defs>
                        <clipPath id="a">
                          <rect class="a" width="16" height="16" />
                        </clipPath>
                      </defs>
                      <g class="b">
                        <g transform="translate(0.219 1.531)">
                          <path
                            class="c"
                            d="M-253.873-160.516a.631.631,0,0,0-.231-.5l-5.731-5.731a.707.707,0,0,0-.516-.249.647.647,0,0,0-.676.641.657.657,0,0,0,.2.481l2.26,2.3,2.723,2.492-2.047-.107h-11.319a.631.631,0,0,0-.659.676.64.64,0,0,0,.659.676H-257.9l2.047-.107-2.723,2.492-2.26,2.278a.688.688,0,0,0-.2.481.647.647,0,0,0,.676.641.664.664,0,0,0,.481-.214l5.766-5.766A.574.574,0,0,0-253.873-160.516Z"
                            transform="translate(269.873 166.994)"
                          />
                        </g>
                      </g>
                    </svg>
                  </div>
                  <div className="col-md-5 col-sm-5 col-6">
                    <div className="to">
                      <div>West Port</div>
                      <div>Houston, TX</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-sm-3 col-12">
                <div className="distance row">
                  <p>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={22}
                      height={22}
                      viewBox="0 0 22 22"
                    >
                      <defs>
                        <clipPath id="prefix__a">
                          <path
                            fill="#1c1c26"
                            stroke="#707070"
                            d="M0 0h22v22H0z"
                          />
                        </clipPath>
                        <style>{".prefix__d{fill:#fff}"}</style>
                      </defs>
                      <g clipPath="url(#prefix__a)">
                        <path
                          d="M18.894 20.968a1.033 1.033 0 01-1.8.687H2.734a3.094 3.094 0 110-6.187h6.9a1.7 1.7 0 000-3.394H4.19a1.031 1.031 0 110-1.375h5.44a3.072 3.072 0 010 6.144h-6.9a1.719 1.719 0 100 3.437h14.36a1.033 1.033 0 011.8.687z"
                          fill="#a5a5a5"
                        />
                        <path d="M6.098 1.109A3.783 3.783 0 00-.36 3.781c0 2.045 1.934 3.743 2.973 4.658.146.129.271.236.37.331a.644.644 0 00.885 0c.1-.095.223-.2.37-.331 1.04-.915 2.973-2.612 2.973-4.658a3.78 3.78 0 00-1.113-2.672z" />
                        <path
                          className="prefix__d"
                          d="M3.421 4.782a1.143 1.143 0 111.143-1.143 1.142 1.142 0 01-1.143 1.143z"
                        />
                        <g>
                          <path d="M20.531 10.733a3.783 3.783 0 00-6.458 2.677c0 2.045 1.934 3.743 2.973 4.658.146.129.271.236.37.331a.644.644 0 00.885 0c.1-.095.223-.2.37-.331 1.035-.915 2.969-2.617 2.969-4.662a3.758 3.758 0 00-1.109-2.673z" />
                          <path
                            className="prefix__d"
                            d="M17.858 14.407a1.143 1.143 0 111.143-1.143 1.142 1.142 0 01-1.143 1.143z"
                          />
                        </g>
                      </g>
                    </svg>{" "}
                    <span id="distances"></span> mi
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default MapInfo;
