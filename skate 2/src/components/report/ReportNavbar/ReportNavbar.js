import React from "react";
import "./ReportNavbar.css";
import user from '../../../../src/images/User.png';
const ReportNavbar = () => {
  return (
    <div className="reportnavbar ">
      <div className="container ">
        <div className="logo"></div>
        <div className="name">
        <img src={user}/>
             <p> Texas Auto Trading co.</p>
        </div>
        <div className="btn">
          <button>Download</button>
        </div>
      </div>
    </div>
  );
};
export default ReportNavbar;
